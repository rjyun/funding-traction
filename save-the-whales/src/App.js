import React, { Component } from 'react';
import {
  Navbar, 
  Icon, 
  Footer
} from 'react-materialize'
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";

import './App.css';
import Home from "./Home"; 


class App extends Component {
  constructor(props) {
    super(props);
    var server
    if (localStorage.getItem('server'))
        //server = localStorage.getItem('server')
        //  server = "https://shufflestats.devzero.be"
        server = "http://localhost:8082"
    else
        //  server = "https://shufflestats.devzero.be"
        server = "http://localhost:8082"

    localStorage.setItem('server', server)

    this.state = {  
        server: server,
    };
  }
  render() {
    return (
      <HashRouter>
        <div>
          <Navbar brand={<NavLink to="/">FLIPSTARTER . CASH</NavLink>} alignLinks="right" className="green lighten-1">
            <NavLink to="/"><Icon left>home</Icon>HOME</NavLink>
          </Navbar>
        
          <div className="App">
            <Route exact path="/" component={Home}/>
          </div>
        </div>
        <Footer
          className="green lighten-2"
          copyrights="&copy; 2020 Copyright fully reserved by FLIPSTARTER . CASH Team"
          links={<ul><li><a className="grey-text text-lighten-3" href="#!">Link 1</a></li><li><a className="grey-text text-lighten-3" href="#!">Link 2</a></li><li><a className="grey-text text-lighten-3" href="#!">Link 3</a></li><li><a className="grey-text text-lighten-3" href="#!">Link 4</a></li></ul>}
          moreLinks={<a className="grey-text text-lighten-4 right" href="#!">More Links</a>}
        >
          <h5 className="white-text">
          FLIPSTARTER . CASH
          </h5>
          <p className="grey-text text-lighten-4">
            You can use rows and columns here to organize your footer content.
          </p>
        </Footer>
      </HashRouter>
    );
  }
}

export default App;
