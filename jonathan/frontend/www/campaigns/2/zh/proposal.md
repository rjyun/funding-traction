## WHO WE ARE

We are **EatBCH**. What started with a $5 donation and a promise for sandwiches quickly became the first cryptocurrency-powered charity in Venezuela, and soon bloomed into an international organization working in over 20 communities in two different countries: Venezula and South Sudan both face difficulties due to extreme currency inflation, and access to a global decentralized currency is one of the only ways local people can interact with the global market in a real, simple and fast way. Bitcoin Cash remains the only way on earth to send micro-donations across borders.

项目介绍

我们是**EatBCH**团队。我们从一笔5美金的捐赠和一个提供食物的承诺起步，很快我们就成为委内瑞拉首个由加密货币驱动的慈善项目。之后，我们发展成一个国际组织，在委内瑞拉和南苏丹的20多个地点都展开了慈善活动。这两个国家都遭受着严重的通货膨胀问题。当地人民只能通过无国界的去中心化货币才能真正参与到全球市场中来，也只有这种方式简单且高效。目前来看，比特币现金是唯一能跨越国界发送小额捐赠的加密货币。

We love to help, and we experience great joy from helping each other. Our volunteers often seem just as grateful as the people they are serving.

我们愿意伸出援手。我们享受互帮互助带来的美好体验。我们的志愿者在帮助他人时内心也充满感激。


## WHAT WE DO

我们的工作

Generous patrons around the world donate Bitcoin Cash. We use that money to buy food from local vendors and our local volunteers distribute the meals to those in need. We then document our proof-of-work with time stamped photographs in a transparent and accountable way.

世界各地都有人慷慨解囊，为我们捐赠比特币现金。我们用善款从当地商户手中购买食物，再由当地的志愿者把食物送给饥肠辘辘的人。我们通过带有时间印记的照片记录日常工作，保证透明，不辜负大家的善意。

## OUR WORK BY THE NUMBERS

用数字记录工作

- **100,000** meals delivered in a two-year period
- **20** locations currently serving cooked meals at least once a week.
- **90%** of our funds goes **directly** to food.
- **40+** dedicated volunteers who deliver food to those in the direst need.

- 过去两年我们共提供了**10万**份餐食。
- **20**个地点每周至少提供一次餐食。
- **90%**的善款都用来购买食物。 
- **40**余位乐于奉献的志愿者把食物给予最需要的人。 

## WHERE WE ARE NOW

我们的现状

**Venezuela**: In our almost two-year period, we have helped many communities across the country and have received amazing and generous support from the entire BCH community. Yet, the situation in Venezuela has not improved at all. Many still depend on the meals we give them every week as the only secured meal they have.

**委内瑞拉**: 过去两年，我们为不同区域的委内瑞拉人提供了帮助，也获得了整个BCH社区的慷慨相助。但是委内瑞拉的情况并没有得到改善。对很多人而言，我们每周送过去的餐食是他们唯一能吃得上的饭。

**South Sudan**: Thanks to a donor push at the end of 2019, we have been able to expand from $100/month at 5 locations to $200/month at 11 locations. Monthly, we require a budget of around 5.28 BCH ($2200) to continue operation. Our current locations are as follows:

**南苏丹**: 2019年底我们为获得捐赠做出了更多工作，从原来的5个地点每月100美金的预算发展到11个地点每月200美金的预算。我们每个月需要大概5.28个BCH（2200美金）来维持运营。目前我们在以下地点展开慈善活动：

- Juba - Junub Open Space tech hub
- Juba - Jebel community with Olympian Kenyi Santino
- Juba - Mahad Internally Displaced Person’s (IDP) Camp
- Juba – Maternity and general support at medical centers
- Juba - Lawanci Orphanage
- Yei - IDPs at the Episcopal Church
- Yei - Single mother’s Vocational Training
- Yei – Maternity support at local medical centers
- Bor - Ataka Tech Hub
- Bor - Blessed Home for Orphans and Delinquents
- Opportunities to expand are unlimited.

- 朱巴市 - Junub Open Space tech hub
- 朱巴市 - Jebel community with Olympian Kenyi Santino
- 朱巴市 - Mahad Internally Displaced Person’s (IDP) Camp
- 朱巴市 – Maternity and general support at medical centers
- 朱巴市 - Lawanci Orphanage
- 耶伊 - IDPs at the Episcopal Church
- 耶伊 - Single mother’s Vocational Training
- 耶伊 – Maternity support at local medical centers
- 博尔 - Ataka Tech Hub
- 博尔 - Blessed Home for Orphans and Delinquents
- 继续扩展工作地点的难度很大。

## How will our funds be distributed?
## 我们将如何分配资金？
**Venezuela**: 3.60 BCH will help us to continue to provide 4,000 meals each month, plus food bags delivered to vulnerable people/families.

**委内瑞拉**: 3.6个BCH能够帮助我们继续每月提供4000份餐食，外加送给弱势群体或家庭的食物袋。

**South Sudan**: 5.28 BCH will help us to continue to provide 6,000 meals each month, plus food bags delivered to vulnerable people/families.

**南苏丹**: 5.28个BCH能够帮助我们继续每月提供6000份餐食，外加送给弱势群体或家庭的食物袋。

## How we measure results
## 我们如何评估工作效果
Since the very moment we started our work, we have used an accountability system to show that we are using funds as effectively as possible. We take pictures of beneficiaries that also include a time stamp with the hash and block height at that moment. We are a team built on our trust networks, and we also work hard to be accountable through verification. This is our 'proof-of-work'.

从启动该项目至今，我们一直坚持执行问责制以确保能够高效使用资金。我们在分配食物时会拍照，并记录当下的时间，哈希和区块高度。我们是一支经得起检验，承担得了问责，值得信任的团队。这就是我们的“工作量”机制。

Coupled with this, as the biggest cryptocurrency-powered charity in both Venezuela and South Sudan, we use the benefit of the blockchain to provide a level of transparency almost never seen before. All of our donations and our spending are recorded, immutably and permanently, in a public ledger for anybody to see.

此外，作为委内瑞拉和南苏丹最大的由加密货币驱动的慈善组织，我们使用区块链技术让慈善达到了前所未有的透明高度。我们的每一笔收入开支都记录在公共账本上，无法修改，无法抹除。