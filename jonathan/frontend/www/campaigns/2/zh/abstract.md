## Help us deliver food for one month
## 帮助我们提供一个月的食物
Over the last two years we have served cooked meals and delivered food bags to people in need and we now ask for your help to secure one more month of operation.

过去两年间，我们为有需要的人们提供餐食和打包好的食物袋。现在，我们需要你的帮助来维持下个月的运营。

- In Venezuela, you support **25** volunteers providing food in **9 locations**.
- In South Sudan, you support **13** volunteers providing food in **11 locations**.

- 在委内瑞拉, 你可以支持25名志愿者为9个不同地点有需要的人送去食物。
- 在南苏丹, 你可以支持13名志愿者为11个不同地点有需要的人送去食物。

We are a group of friends doing what we can to help our neighbors in this time of uncertainty. What better way to bond with your neighbors than over a hot meal prepared with love?

我们聚到一起，帮助邻里的贫困人们抵抗不确定性。还有什么比一顿饱含爱意的、热乎乎的餐食更能维系邻里间的情感纽带吗？